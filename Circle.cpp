/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <iostream>
using namespace std;

namespace Student1 {

}

/* Question3: In Circle class, create an equals function which returns boolean to compare circle4 and circle5 objects
*and print if they are equal or not. */



Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r) {
	this->r = r;
}

void Circle::setR(int r) {
	this->r = r;
}

double Circle::getR() {
	return r;
}

double Circle::calculateCircumference() {
	return PI * 2 * r;
}

double Circle::calculateArea() {
	return PI * r * r;
}

bool Circle::equals(double c4, double c5) {
	if (c4 == c5)
	{
		cout << "they are equal.";
		return 1;
	}
	else
	{
		cout << "they are not equal.";
		return 0;
	}
}
