/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	Circle(double);
	virtual ~Circle();
	void setR(double);
	void setR(int);
	double getR();
	double calculateCircumference();
	double calculateArea();
	bool equals(double c4, double c5);
private:
	double r;
	const double PI = 3.14; 
};
#endif /* CIRCLE_H_ */
